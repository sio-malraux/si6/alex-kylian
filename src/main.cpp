// auteurs : alex renou et kylian trigoust

#include <iomanip>
#include <iostream>
#include <libpq-fe.h> // inclusion du fichier d'entête libpq

int main()
{
  // 1 - vérification de la connectivité réseau du serveur postgresql
  const char chaine_connexion[] = "host=postgresql.bts-malraux72.net connect_timeout=10";
  PGPing test_ping;
  
  test_ping = PQping(chaine_connexion);

  if (test_ping == PQPING_OK)
  {
    std::cout << "La connexion au serveur de base de données a été établie avec succès." << std::endl;

    // 2 - établissement de la connexion au serveur
    PGconn *connexion;
    ConnStatusType statut;
    
    connexion = PQconnectdb(chaine_connexion);
    statut = PQstatus(connexion);
    
    if (statut == CONNECTION_OK)
    {
      // 3 - affichage des informations de connexion au serveur postgresql
      char
        *hote,
        *utilisateur,
        *pass,
        *bdd,
        *port;
      const char
        *param_encodage = "server_encoding",
        *encodage;
      int
        carac,
        compteur,
        ssl,
        version_protocole,
        version_serveur,
        version_libpq;
      bool
        loop;
      std::string
        pass_cache,
        retour_ssl;
              
      hote = PQhost(connexion);
      utilisateur = PQuser(connexion);
      pass = PQpass(connexion);
      bdd = PQdb(connexion);
      port = PQport(connexion);
      ssl = PQsslInUse(connexion);
      retour_ssl = (ssl) ? "true" : "false";
      param_encodage = "server_encoding";
      encodage = PQparameterStatus(connexion, param_encodage);
      version_protocole = PQprotocolVersion(connexion);
      version_serveur = PQserverVersion(connexion);
      version_libpq = PQlibVersion();

      carac = compteur = 0;
      loop = true;

      while (loop)
      {
        if (pass[carac] != '\0')
        {
          ++compteur;
        }
        else
        {
          loop = false;

          for (carac = 0; carac < compteur; ++carac)
          {
            pass_cache += '*';
          }
        }
        
        ++carac;
      }
        
      std::cout << "La connexion au serveur de base de données '" << hote << "' a été établie avec les paramètres suivants :"
                << std::endl << "* utilisateur : " << utilisateur
                << std::endl << "* mot de passe : " << pass_cache
                << std::endl << "* base de données : " << bdd
                << std::endl << "* port TCP : " << port
                << std::endl << "* chiffrement SSL : " << retour_ssl
                << std::endl << "* encodage : " << encodage
                << std::endl << "* version du protocole : " << version_protocole
                << std::endl << "* version du serveur : " << version_serveur
                << std::endl << "* version de la bibliothèque 'libpq' du client : " << version_libpq
                << std::endl << std::endl;

      // 4 - requête sql de sélection
      PGresult *requete;
      ExecStatusType resultat;
      
      requete = PQexec(connexion, "SELECT * FROM elevage.race INNER JOIN elevage.animal on animal.race_id = race.id WHERE animal.sexe = 'F' AND race.nom = 'Singapura';");
      resultat = PQresultStatus(requete);

      // 5 - exécution et gestion des erreurs de la requête
      if (resultat == PGRES_EMPTY_QUERY)
        std::cout << "La chaîne envoyée au serveur était vide." << std::endl;
      else if (resultat == PGRES_COMMAND_OK)
        std::cout << "Fin avec succès d'une commande ne renvoyant aucune donnée." << std::endl;

      // 6 - affichage des données concernant la requête
      else if (resultat == PGRES_TUPLES_OK)
      {
        int
          nb_lignes = PQntuples(requete),
          nb_colonnes = PQnfields(requete),
          nb_carac = 0,
          nb_carac_max = 0;
        std::string
          separateur = " | ";
        bool
          entete = false;

        // 7 - normalisation de la longueur des champs
        for (int ligne = 0; ligne < nb_lignes; ++ligne)
        {
          for (int colonne = 0; colonne < nb_colonnes; ++colonne)
          {
            char *champs = PQgetvalue(requete, ligne, colonne);

            for (int longueur = 0; champs[longueur] != '\0'; ++longueur)
              ++nb_carac;

            if (nb_carac > nb_carac_max)
              nb_carac_max = nb_carac;

            nb_carac = 0;
          }

          // 8 - affichage des champs du résultat de la requête
          if (entete == false)
          {
            std::cout << separateur << std::setw(nb_carac_max) << std::left << "id"
                      << separateur << std::setw(nb_carac_max) << std::left << "nom"
                      << separateur << std::setw(nb_carac_max) << std::left << "espece_id"
                      << separateur << std::setw(nb_carac_max) << std::left << "description"
                      << separateur << std::setw(nb_carac_max) << std::left << "prix"
                      << separateur << std::setw(nb_carac_max) << std::left << "id"
                      << separateur << std::setw(nb_carac_max) << std::left << "sexe"
                      << separateur << std::setw(nb_carac_max) << std::left << "date_naiss"
                      << separateur << std::setw(nb_carac_max) << std::left << "nom"
                      << separateur << std::setw(nb_carac_max) << std::left << "commentaires"
                      << separateur << std::setw(nb_carac_max) << std::left << "espece_id"
                      << separateur << std::setw(nb_carac_max) << std::left << "race_id"
                      << separateur << std::setw(nb_carac_max) << std::left << "mere_id"
                      << separateur << std::setw(nb_carac_max) << std::left << "pere_id"
                      << std::endl;

            entete = true;
          }
            
          for (int colonne = 0; colonne < nb_colonnes; ++colonne)
          {
            // 9 - alignement à gauche de l'affichage des données
            std::cout << separateur << std::setw(nb_carac_max) << std::left << PQgetvalue(requete, ligne, colonne);
          }
        }
        
        std::cout << std::endl << std::endl << "=> L'éxécution de la requête SQL a retourné " << nb_lignes << " enregistrements." << std::endl;
      }
      else if (resultat == PGRES_COPY_OUT)
        std::cout << "Début de l'envoi d'un flux de données." << std::endl;
      else if (resultat == PGRES_COPY_IN)
        std::cout << "Début de la réception d'un flux de données." << std::endl;
      else if (resultat == PGRES_BAD_RESPONSE)
        std::cout << "La réponse du serveur n'a pas été comprise." << std::endl;
      else if (resultat == PGRES_NONFATAL_ERROR)
        std::cout << "Une erreur non fatale est survenue." << std::endl;
      else if (resultat == PGRES_FATAL_ERROR)
        std::cout << "Une erreur fatale est survenue." << std::endl;
      else if (resultat == PGRES_COPY_BOTH)
        std::cout << "Lancement du transfert de données Copy In/Out." << std::endl;
      else if (resultat == PGRES_SINGLE_TUPLE)
        std::cout << "La structure PGresult contient une seule ligne de résultat provenant de la commande courante." << std::endl;
      else
        std::cout << "Erreur inconnue !" << std::endl;
    }
    else
    {
      std::cerr << "Connexion impossible :(" << std::endl;
    }
  }

  else
  {
    std::cerr << "Malheureusement le serveur n'est pas joignable. Vérifier la connectivité." << std::endl;
  }

  return 0;
}
